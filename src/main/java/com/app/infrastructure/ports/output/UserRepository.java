package com.app.infrastructure.ports.output;

import com.app.domain.model.UserModel;
import org.springframework.stereotype.Repository;


public interface UserRepository {

    UserModel findById(Long id);

    UserModel save(UserModel user);

}
