package com.app.infrastructure.adapters.input.rest.mapper;

import com.app.domain.model.UserModel;
import com.app.infrastructure.adapters.input.rest.dto.UserJson;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel="spring", imports = {UserJson.class, UserModel.class})
public interface UserRestMapper {

    UserRestMapper MAPPER = Mappers.getMapper(UserRestMapper.class);

    UserJson toDto(UserModel user);

    UserModel toDomain(UserJson userJson);

}
