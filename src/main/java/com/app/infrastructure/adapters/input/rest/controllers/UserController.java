package com.app.infrastructure.adapters.input.rest.controllers;

import com.app.domain.usecases.UserUseCase;
import com.app.infrastructure.adapters.input.rest.dto.UserJson;
import com.app.infrastructure.adapters.input.rest.mapper.UserRestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserUseCase userUseCase;

    @GetMapping("users/{id}")
    public ResponseEntity<UserJson> getUserById(@PathVariable Long id) {

        return new ResponseEntity<>(UserRestMapper.MAPPER.toDto(userUseCase.getUser(id)), HttpStatus.OK);

    }

    @PostMapping("users")
    public ResponseEntity<UserJson> saveUser(@RequestBody UserJson userDto) {

        return new ResponseEntity<>(UserRestMapper.MAPPER.toDto(userUseCase.saveUser(UserRestMapper.MAPPER.toDomain(userDto))),
                HttpStatus.CREATED);

    }

}
