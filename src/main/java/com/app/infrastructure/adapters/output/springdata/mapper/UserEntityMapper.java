package com.app.infrastructure.adapters.output.springdata.mapper;

import com.app.domain.model.UserModel;
import com.app.infrastructure.adapters.output.springdata.dbo.UserDbEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", imports = {UserModel.class, UserDbEntity.class})
public interface UserEntityMapper {
  UserEntityMapper MAPPER = Mappers.getMapper(UserEntityMapper.class);
  UserModel toDomain(UserDbEntity userEntity);

  UserDbEntity toDbo(UserModel user);

}
