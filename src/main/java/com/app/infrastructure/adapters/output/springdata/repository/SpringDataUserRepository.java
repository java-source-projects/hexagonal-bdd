package com.app.infrastructure.adapters.output.springdata.repository;


import com.app.infrastructure.adapters.output.springdata.dbo.UserDbEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

public interface SpringDataUserRepository extends JpaRepository<UserDbEntity, Long> {
}
