package com.app.infrastructure.adapters.output.springdata.repository;

import com.app.domain.model.UserModel;
import com.app.infrastructure.adapters.output.springdata.mapper.UserEntityMapper;
import com.app.infrastructure.ports.output.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDboRepository implements UserRepository {

    @Autowired
    private SpringDataUserRepository springDataUserRepository;

    @Override
    public UserModel findById(Long id) {
        return UserEntityMapper.MAPPER.toDomain(springDataUserRepository.findById(id).orElseThrow());
    }

    @Override
    public UserModel save(UserModel user) {

        return UserEntityMapper.MAPPER.toDomain(springDataUserRepository.save(UserEntityMapper.MAPPER.toDbo(user)));

    }
}
