package com.app.application;

import com.app.domain.model.UserModel;
import com.app.domain.usecases.UserUseCase;
import com.app.infrastructure.ports.output.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserUseCaseImpl implements UserUseCase {

    @Autowired
    private UserRepository userRepository;

    public UserModel getUser(Long id) {

        return userRepository.findById(id);
    }

    public UserModel saveUser(UserModel user) {

        return userRepository.save(user);

    }
}
