package com.app.domain.usecases;

import com.app.domain.model.UserModel;
import org.springframework.stereotype.Component;

public interface UserUseCase {
    UserModel getUser(Long id);

    UserModel saveUser(UserModel user);
}
